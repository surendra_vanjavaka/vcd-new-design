$(document).ready(function() {
  
  $(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 90
    }, 500);
  });

  $('.kitchen-sink-left a').on('click', function() {
    $('.kitchen-sink-left a').removeClass('active');
    $(this).addClass('active');
  });


  $(window).on('scroll', function() {
      var scrollTop = $(this).scrollTop();

      $('.background1, .background2').each(function() {
          var topDistance = $(this).offset().top;

          if ( (topDistance+100) < scrollTop ) {
              alert( $(this).text() + ' was scrolled to the top' );
          }
      });
  });

  /* scroll down to hash/id on pageload */
  if (location.hash) {
    var hash = window.location.hash;     
    if ($(hash).length != 0) {
      /* scroll down to hash/id on pageload */
      $("html, body").delay(500).animate({scrollTop: $(hash).offset().top - 90}, 500);
    }
  }

}); // end: jquery