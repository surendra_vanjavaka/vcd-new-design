$(document).ready(function() {


  /*
  --------------------------------------
  EDITOR-SPECIFIC FUNCTINS
  --------------------------------------
  */


  /* EDITOR: show/hide left sidebar (v-editor-header) in editor */
  $('.v-editor-header-toggle').on('click', function() {
    $('header.v-editor-header').toggleClass('v-hidden');
  });

  $('header .v-show-menu').on('click', function () {
      $('header.v-editor-header').removeClass('v-hidden');
    });

 $('aside .v-show-menu').on('click', function () {
  //  alert('dfdf');
   $('aside.v-fixed').removeClass('v-hidden');
   $('.v-editor-main').removeClass('v-hidden');
 });
 
 $('.v-show-menu').on('click', function () {
  //  alert('dfdf');
   $('aside.v-fixed').removeClass('v-hidden');
   $('.v-editor-main').removeClass('v-hidden');
 });
     


  /* EDITOR: show/hide tab-specific right sidebar (.v-aside.v-fixed) */
  $('.v-editor-aside-fixed-toggle').on('click', function() {
    $(this).parents('aside.v-fixed').toggleClass('v-hidden 111'); 

    if ($(this).parents('aside.v-fixed').prev('.v-editor-main').hasClass('v-hidden')) {
      $(this).parents('aside.v-fixed').prev('.v-editor-main').removeClass('v-hidden');
    }
    else {
      $(this).parents('aside.v-fixed').prev('.v-editor-main').addClass('v-hidden');
    }

  });

  


  /* EDITOR: show/hide asides from link in canvas */
  $('.v-editor-main-content').on('click', '.v-toggle-aside', function() {
    var asideID = $(this).attr("name");
    var asideIDclass = 'show-' + asideID;

    if ( ($('.v-aside.v-open').hasClass(asideIDclass)) ) {   
      $(this).removeClass('v-active');  
      $('.' + asideIDclass).removeClass('v-open');
      //$('.v-aside-open-overlay').hide(); /* EDITOR: if there is need to close aside when click anywhere on page, remove comments around "$('.v-aside-open-overlay')" in 5 places */
      if ($(this).parents('.v-editor-main').next('.v-aside').hasClass('v-hidden')) {
        $(this).parents('.v-editor-main').addClass('v-hidden');
      }
    }
    else {
      $('.v-toggle-aside.v-active').removeClass('v-active');
      $('.v-aside.v-open').removeClass('v-open');

      $(this).addClass('v-active');  
      $('.' + asideIDclass).addClass('v-open');
      //$('.v-aside-open-overlay').show();
      if ($(this).parents('.v-editor-main').hasClass('v-hidden')) {
        $(this).parents('.v-editor-main').removeClass('v-hidden');
      }
    }

  });


  /* EDITOR: close aside when click "DONE" button in aside */
  $('.v-aside-head .btn').on('click', function() {
    /*$('.v-aside-open-overlay').trigger('click');*/
    $('.v-aside').removeClass('v-open');
    $('.v-toggle-aside').removeClass('v-active');
    $('aside.v-fixed').each(function() {
      if ($(this).hasClass('v-hidden')) {
        $(this).prev('.v-editor-main').addClass('v-hidden');
      }
    });
  });


  /* EDITOR: hide aside when click anywhere in canvas. Remember to remove comments in previous function */
  /*$('.v-aside-open-overlay').on('click', function() {
    $(this).hide();
    $('.v-aside').removeClass('v-open');
    $('.v-toggle-aside').removeClass('v-active');
  });*/


  /* EDITOR: hides aside when navigate to other tab in left sidebar */
  $('.v-editor-header-body .nav-item > .nav-link').on('click', function() {
    $('.v-aside.v-open').removeClass('v-open');
    //$('.v-editor-main').removeClass('v-hidden');
  });


  /* EDITOR: when navigate to other tab in left sidebar, make sure "aside.v-fixed" is visible */
  $('.v-editor-header .nav-item').on('click', function() {
    //$('.v-aside-open-overlay').trigger('click');
    $('aside.v-fixed.v-hidden').removeClass('66');
  });


  /* EDITOR: temporary. When click headline/name in editor, that opens correct tab for edit */
  $('.v-trigger-settings').on('click', function() {
    $('#item4-tab').trigger('click');
  });
  

  /* EDITOR: When linking from general page to specific tab in editor */
  let url = location.href.replace(/\/$/, "");
  if (location.hash) {
    const hash = url.split("#");
    $('.v-editor-header-body a[href="#'+hash[1]+'"]').tab("show");
    url = location.href.replace(/\/#/, "#");
    history.replaceState(null, null, url);
    setTimeout(() => {
      $(window).scrollTop(0);
    }, 400);
  }    
  $('a[data-toggle="tab"]').on("click", function() {
    let newUrl;
    const hash = $(this).attr("href");
    newUrl = url.split("#")[0] + hash;
    newUrl += "/";
    history.replaceState(null, null, newUrl);
  });


  /* EDITOR: for API-tab in editor, in list of inputs/outputs, click headline will show/hide corresponding list */
  $('body').on('click', '.v-showhide', function() {
    $(this).toggleClass('v-closed');
    $(this).next('ul').slideToggle();
  });


  /* EDITOR: for API-tab in editor, search/filter content in each column of APIs */
  jQuery.expr[':'].contains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
  };
  $('.v-select-api-wrapper').on('input','.v-autocomplete', function() {
    var txt = $(this).val();
    $(this).parents('.v-select-api-search').next('.v-select-api-list').find('li:contains("'+txt+'")').slideDown();
    $(this).parents('.v-select-api-search').next('.v-select-api-list').find('li:not(:contains("'+txt+'"))').slideUp();
  });
  $('.v-general-main').on('input','.v-autocomplete2', function() {
    var txt = $(this).val();
    $(this).parents('.v-general-main-filters').next('.v-general-main-content').find('.table tbody').find('tr:contains("'+txt+'")').slideDown();
    $(this).parents('.v-general-main-filters').next('.v-general-main-content').find('.table tbody').find('tr:not(:contains("'+txt+'"))').slideUp();
  });


  /* EDITOR: only as example for functionality. Activate SAVE button and last-saved, if save takes time  */
  $('.v-editor-main-content').on('click', '.v-change-api, .v-cancel-app-api', function() {
   
    $('.v-header-actions-primary .btn-primary').removeClass('disabled');
    $('.v-header-actions-primary .v-last-saved').slideUp();
    setTimeout(function(){
      $('.v-header-actions-primary .btn-primary').addClass('disabled');
      $('.v-header-actions-primary .v-last-saved').slideDown();
    }, 3000);
  });
   $('.v-editor-main-content').on('click', '.v-cancel-app-api.v-settings-view', function() {
    $('aside.v-fixed').removeClass('v-hidden');
   $('.v-editor-main').removeClass('v-hidden');
  });





  /*
  --------------------------------------
  GENRAL-SPECIFIC FUNCTINS
  --------------------------------------
  */


  /* GENERAL: When use expandable boxes, show/hide expanded content */
  $('.v-expandable-summary').on('click', function() {
    if ($(this).parent('li').hasClass('v-open')) {
      $(this).parent('li').removeClass('v-open');
      $(this).parent('li').find('.v-expandable-details').removeClass('v-open');
    }
    else {
      $('.v-expandable-list').find('li').removeClass('v-open');
      $('.v-expandable-list').find('li').find('.v-expandable-summary').removeClass('v-open');
      $(this).parent('li').addClass('v-open');
      $(this).find('.v-expandable-details').addClass('v-open');
    }
  });


  /* GENERAL, TABLESORTER: Sortable table headers with jQuery-plugin tablesorter */
  $('.v-with-sortable-headers').tablesorter({
      selectorSort : 'div.tablesorter-header-inner', 
      sortInitialOrder: 'asc', // Sorted desc default
      sortRestart : true //  Start with sortInitialOrder on each columns
  });

  /* GENERAL, TABLESORTER: adds "sort-this-column"-title-tag to each clickable header in tablesorter */
  $('.v-with-sortable-headers th:not(".sorter-false")').find('div[class="tablesorter-header-inner"]').attr('title', 'Sort this column');
  
  /* GENERAL, TABLESORTER: sort table from select-dropdown in "v-general-main-filters" */
  $('.v-sort-table').change(function(){
    var column = parseInt($(this).val(), 10),
      direction = 0, // 0 = descending, 1 = ascending
      sort = [[ column, direction ]];
    if (column >= 0) {
      $('table').trigger("sorton", [sort]);
    }
  });


  /* GENERAL: Make v-general-main-toolbar bottom-sticky if content taller than window height */
  $( window ).on('resize load', function() {
    if ($('.v-general-main-toolbar').length == 1) {

      $('.v-general-main-toolbar').removeClass('v-sticky');

      var elementPosition = $('.v-general-main-toolbar').offset().top;
      var windowHeight = $(window).height() - 68;

      if (elementPosition >= windowHeight) {
        $('.v-general-main-toolbar').addClass('v-sticky');
      }

    }
  });


  /* GENERAL: only as example for functionality. Activate SAVE button and last-saved, if save takes time  */
  $('.v-add-users').on('click', function() {
    $('.v-last-saved').fadeOut();
    $('.v-general-main-toolbar .btn-primary').prop('disabled', false);
    setTimeout(function(){
      $('.v-general-main-toolbar .btn-primary').prop('disabled', true);
      $('.v-last-saved').fadeIn();
    }, 3000);   
  });


  /* GENERAL: On mobile,   */
  $('.v-general-header .navbar-toggler').on('click', function() {
    if ($('.v-general-header .dropdown').length == 1) {
      $('.v-general-header .nav-item.dropdown').removeClass('dropdown');
      $('.v-general-header .dropdown-toggle').hide();
      $('.v-general-header .dropdown-menu').addClass('show');
    }
  });




  /*
  --------------------------------------
  FUNCTINS FOR BOTH GENERAL AND EDITOR
  --------------------------------------
  */


  /* GENERAL + EDITOR: toggle favorite */
  $('.v-favorite').on('click', function() {
    $(this).toggleClass('v-chosen');

  });



  /* GENERAL + EDITOR: initialize all Bootstrap tooltip */
  $('[data-toggle="tooltip"]').tooltip();
 

  /* GENERAL + EDITOR: initialize all Bootstrap popouver */
  $('[data-toggle="popover"]').popover({
    trigger: 'focus' /* Click outside box to close popover */
  });


  /* GENERAL + EDITOR: Make modal open on pageload, and force user to chose */
  $( window ).on('load', function() {
    if ($('#force-user-to-chose').length == 1) {
      $('#force-user-to-chose').modal({backdrop: 'static', keyboard: false}) 
    }
  });


  /* GENERAL + EDITOR: simple drag&drop width jQuery UI, not sure if this will be used */
  $('.v-sortable').sortable({
    cancel: ".no-draggable",
    refreshPositions: true,
    cursor: "crosshair",
    snap: true
  });
  $('.v-sortable').disableSelection();



  /* GENERAL + EDITOR: For making tables with thead, mobile friendly */
  $('.v-table').each(function() {

      if ( $(this).find('thead').length > 0 ) {
          var $th = $(this).find('th');
          var $tr = $(this).find('tr');

          $tr.each(function(){
              $(this).find('td').each(function(index) {

                  if ( !$(this).hasClass('empty-data-attr')) {
                      $(this).attr('data-attr', $th.eq(index).text());
                  }

              });
          });        
      }
      else {
          $(this).find('tbody tr td').addClass('hide-before-pseudo');              
      }

  });

/*infobox*/
  $('.bar_chart_view').show();
    $('.info_box_view').show();
       $('.bar_chart_view_tabs').hide();
   $('.v-create-widget .info_box').on('click', function() {
          $('.bar_chart_view').hide();
    $('.v_common_toogle').removeClass('box_hidden');
      $('.v_common_toogle').addClass('widget_hidden');
 
  });
     $('.v-create-widget .bar_chart').on('click', function() {
     
        $('.v-editor-main.v-hidden').addClass('overlay_modal');
         $('.v-editor-main-header').addClass('overlay_modal');
   
          $('.info_box_view').hide();
          
                $('.bar_chart_view_tabs').hide();
       
    $('.v_common_toogle').removeClass('box_hidden');
      $('.v_common_toogle').addClass('widget_hidden');
        $('.v-editor-main.v-hidden').addClass('overlay_modal');
        
         $('.v-editor-main-header').addClass('overlay_modal');
 
  });

   

  
   $('.v-create-widget-footer .cancel').on('click', function() {

    //    setTimeout(() => {
    //       alert("12345");
    // }, 400);
      $('.v-editor-main.v-hidden').removeClass('overlay_modal');
      
         $('.v-editor-main-header').removeClass('overlay_modal');
    $('.v_common_toogle').removeClass('widget_hidden');
      $('.v_common_toogle').addClass('box_hidden');
 
  });
    $('.v-create-widget-footer .create').on('click', function() {

    //    setTimeout(() => {
    //       alert("12345");
    // }, 400);
    $('.v_common_toogle').removeClass('widget_hidden');
      $('.v_common_toogle').addClass('box_hidden');
       $('.v_common_toogle').addClass('created_widget');
       $('aside.v-fixed').removeClass('v-hidden');
 
  });
      $('.v-create-widget-footer .bar_Create').on('click', function() {

// alert("111");
      $('.bar_chart_view_tabs').show();
    // $('.v_common_toogle').removeClass('widget_hidden');
    //   $('.v_common_toogle').addClass('box_hidden');
    //    $('.v_common_toogle').addClass('created_widget');
       $('aside.v-fixed').removeClass('v-hidden');
 
  });

 

});


document.querySelector('.custom-select-wrapper').addEventListener('click', function() {
    this.querySelector('.custom-select').classList.toggle('open');

    for (const option of document.querySelectorAll(".custom-option")) {
    option.addEventListener('click', function() {
        if (!this.classList.contains('selected')) {
            this.parentNode.querySelector('.custom-option.selected').classList.remove('selected');
            this.classList.add('selected');
            this.closest('.custom-select').querySelector('.custom-select__trigger span').innerHTML = this.innerHTML;
        }
    })
}
})